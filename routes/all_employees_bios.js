const { query } = require('express');
var express = require('express');
var router = express.Router();
var database = require('../database/database');

router.get('/', function(req, res, next) {
  filter = req.query.filter;
  if (filter === undefined) {
    /* GET all employees' bios */
    send_all_employees_bios(res);
  } else {
    /* GET filtered employees' bios by filter keyword(s) */
    send_filtered_employees_bios(res);
  }
});

function send_filtered_employees_bios(res) {
  result = database.get_filtered_employee_bios(filter);
  if (result.length != 0) {
    res.send(result);
  } else {
    res.send({});
  }
}

function send_all_employees_bios(res) {
  res.send(database.get_all_employees_bios());
}

module.exports = router;
