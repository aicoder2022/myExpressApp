var express = require('express');
var router = express.Router();
var database = require('../database/database');

/* GET specific employee's bio by slug */
router.get('/', function(req, res, next) {
  slug = req.baseUrl.slice(1).toLowerCase()

  result = database.get_employee_bio_by_slug(slug)
  if (result.length != 0) {
    res.send(result);
  } else {
    res.sendStatus(404);
  }
});

module.exports = router;
