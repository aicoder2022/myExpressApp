var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var http = require('http');
var cors = require('cors');

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

var indexRouter = require('./routes/index');
var employeeBiosRouter = require('./routes/all_employees_bios');
var employeeBioBySlugRouter = require('./routes/employee_bio_by_slug');

var app = express();

app.use(cors());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', indexRouter);
app.use('/bios', employeeBiosRouter);
app.use('/:slug', employeeBioBySlugRouter);

// swagger
indexRouter.use('/api-docs', swaggerUi.serve);
indexRouter.get('/api-docs', swaggerUi.setup(swaggerDocument));

module.exports = app;