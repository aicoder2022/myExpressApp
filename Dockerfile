FROM node:lts-alpine

WORKDIR /usr/src/app

COPY [ "tmp", "./" ]

RUN echo "node version: "
RUN node --version
RUN echo "npm version: "
RUN npm --version

# Development: do build and run tests
ENV NODE_ENV=development
RUN npm install
RUN npm test
RUN rm -fr node_modules tests

# Production: run build as final image
ENV NODE_ENV=production
RUN npm install --production

ARG PORT
EXPOSE ${PORT}
RUN chown -R node /usr/src/app
USER node
CMD ["npm", "start"]
