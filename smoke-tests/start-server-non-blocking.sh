#!/bin/bash

set -eou pipefail

TARGET="${1:-"--native"}"

checkDockerLogs() {
    DOCKER_RUNNER_CMD=$1
    OUTPUT_LOG=$2
    ${DOCKER_RUNNER_CMD} --showLogs > ${OUTPUT_LOG}
    grep -q "node ./bin/www" ${OUTPUT_LOG}
}

checkNodeLogs() {
    OUTPUT_LOG=$1
    grep -q "node ./bin/www" ${OUTPUT_LOG}
}

if [[ "${TARGET}" == '--docker' ]]; then
    CURRENT_SOURCE_FOLDER="$( cd $(dirname $0) && pwd )"
    DOCKER_RUNNER_CMD="${CURRENT_SOURCE_FOLDER}/../docker-runner.sh"
    ${DOCKER_RUNNER_CMD} --detach --runContainer

    OUTPUT_LOG="node_server_in_docker_container.log"
    COMMAND_TO_CHECK_OUTPUT_LOGS="checkDockerLogs ${DOCKER_RUNNER_CMD} ${OUTPUT_LOG}"
else
    OUTPUT_LOG="node_server.log"
    npm start > ${OUTPUT_LOG} &
    COMMAND_TO_CHECK_OUTPUT_LOGS="checkNodeLogs ${OUTPUT_LOG}"
fi

TARGET="$(echo ${TARGET} | tr -d '-')"
echo "Starting server (${TARGET})" >&2
while ! $(${COMMAND_TO_CHECK_OUTPUT_LOGS})
do
    sleep .1
    echo -en "."
done

echo "";
cat ${OUTPUT_LOG} >&2
rm ${OUTPUT_LOG}
echo -e "Server has started (${TARGET})." >&2
lsof -Pan -c node -i
sleep 0.5
exit 0