RED="\033[0;31m"
GREEN="\033[0;32m"
DEFAULT="\033[0m"
PASSED_TOKEN="${GREEN}PASSED${DEFAULT}"
FAILED_TOKEN="${RED}FAILED${DEFAULT}"

print_test_results() {
    error_code="$1"
    test_description="$2"

    echo "";
    if [[ "${error_code:-0}" == "0" ]]; then
        echo "${PASSED_TOKEN}: ${test_description} was successful." >&2
    else
        echo "${FAILED_TOKEN}: ${test_description} was NOT successful." >&2
    fi
    echo "";
}
