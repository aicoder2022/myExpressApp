#!/bin/bash

set -eou pipefail

CURRENT_SOURCE_FOLDER="$( cd $(dirname $0) && pwd )"
source "${CURRENT_SOURCE_FOLDER}/common.sh"
ENDPOINT_SERVER="localhost:8080"

run_swagger_docs_test() {
    echo ""; echo "--- Running smoke test(s) against the Swagger endpoint" >&2

    echo ""; echo "~~~ Test: fetching swagger docs interface" >&2
    TEMP_OUTPUT_FILE=$(mktemp)
    exit_code=$(curl -L ${ENDPOINT_SERVER}/api-docs > ${TEMP_OUTPUT_FILE} && \
                grep -q -o "Swagger UI" ${TEMP_OUTPUT_FILE} || echo $?)

    print_test_results "${exit_code:-0}" "swagger docs interface test"
    echo "${exit_code}"
}

test_exit_code="$(run_swagger_docs_test | tr -d '\n')"
exit ${test_exit_code}