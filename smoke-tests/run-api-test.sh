#!/bin/bash

set -eou pipefail

CURRENT_SOURCE_FOLDER="$( cd $(dirname $0) && pwd )"
source "${CURRENT_SOURCE_FOLDER}/common.sh"
ENDPOINT_SERVER="localhost:8080"
BIOS_ENDPOINT_SERVER="${ENDPOINT_SERVER}/bios"

run_fetch_all_bios_test() {
    ### Given
    echo ""; echo "~~~ Test: making REST call 'GET /bios'" >&2
    TEMP_OUTPUT_FILE="$(mktemp)"

    ### When
    exit_code=$(curl ${BIOS_ENDPOINT_SERVER} > \
                ${TEMP_OUTPUT_FILE} &&         \
                grep -q -o \"urn\"             \
                ${TEMP_OUTPUT_FILE} >&2 || echo $?)

    ### Then
    exit_code="${exit_code:-0}"
    print_test_results "${exit_code}" "fetch all bios API test"
    echo "${exit_code}"
}

run_fetch_bio_via_slug_test() {
    ### Given
    echo ""; echo "~~~ Test: making REST call 'GET /:slug'" >&2
    TEMP_OUTPUT_FILE="$(mktemp)"
    slug="luke-saunders"

    ### When
    exit_code=$(curl ${ENDPOINT_SERVER}/${slug} > \
                ${TEMP_OUTPUT_FILE} &&            \
                grep -q -o \"urn\"                \
                ${TEMP_OUTPUT_FILE} >&2 || echo $?)

    ### Then
    exit_code="${exit_code:-0}"
    print_test_results "${exit_code}" "fetch bios via slug API test"
    echo "${exit_code}"
}

run_fetch_bio_by_filter_test() {
    ### Given
    echo ""; echo "~~~ Test: making REST call 'GET /bios?filter=<text>'" >&2
    TEMP_OUTPUT_FILE="$(mktemp)"
    filterBy="Luke"

    ### When
    exit_code=$(curl ${BIOS_ENDPOINT_SERVER}\?filter\=${filterBy} > \
                ${TEMP_OUTPUT_FILE} &&                              \
                grep -q -o \"${filterBy} ${TEMP_OUTPUT_FILE} >&2 || \
                echo $?)

    ### Then
    exit_code="${exit_code:-0}"
    print_test_results "${exit_code}" "fetch bios via a filter test"
    echo "${exit_code}"
}

run_all_tests() {
    echo "";
    echo "--- Running smoke tests against the API endpoints" >&2

    test1_exit_code="$(run_fetch_all_bios_test | tr -d '\n')"
    echo "";
    test2_exit_code="$(run_fetch_bio_via_slug_test | tr -d '\n')"
    echo "";
    test3_exit_code="$(run_fetch_bio_by_filter_test | tr -d '\n')"

    if [[ "${test1_exit_code}" == "0" ]] && \
       [[ "${test2_exit_code}" == "0" ]] && \
       [[ "${test3_exit_code}" == "0" ]]; then
        exit 0
    else
        exit 1
    fi
}

run_all_tests