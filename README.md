# A simple node.js backend server

![node](https://img.shields.io/badge/node-v17.9.0-green) ![docker](https://img.shields.io/badge/docker-support-navy)

**Project health**: main branch: [![pipeline (main branch)](https://gitlab.com/ANDigital/hamilton/myExpressApp/badges/main/pipeline.svg)](https://gitlab.com/ANDigital/hamilton/myExpressApp/-/commits/main) [![coverage report](https://gitlab.com/ANDigital/hamilton/myExpressApp/badges/main/coverage.svg)](https://gitlab.com/ANDigital/hamilton/myExpressApp/-/commits/main) develop branch: [![pipeline (develop branch)](https://gitlab.com/ANDigital/hamilton/myExpressApp/badges/develop/pipeline.svg)](https://gitlab.com/ANDigital/hamilton/myExpressApp/-/commits/develop) [![coverage report](https://gitlab.com/ANDigital/hamilton/myExpressApp/badges/develop/coverage.svg)](https://gitlab.com/ANDigital/hamilton/myExpressApp/-/commits/develop)

![Quality gate: dev-tests](https://img.shields.io/badge/QualityGate-Dev%20tests-blue) ![Quality gate: test-coverage](https://img.shields.io/badge/QualityGate-Test%20coverage-blue) ![Quality gate: post-deployment-tests](https://img.shields.io/badge/QualityGate-Post%20deployment%20tests-blue) ![Quality gate: docker image build](https://img.shields.io/badge/QualityGate-Docker%20image%20build-blue)

![Documentation](https://img.shields.io/badge/Documentation%20via-README%20|%20FAQs%20|%20Swagger%20docs-lightgrey)

A REST API node.js server that returns bios of employees.

- [Prerequisites](#prerequisites)
- [Local/native environment](#localnative-environment)
  - [Install app](#install-app)
  - [Run app](#run-app)
- [Docker environment](#docker-environment)
  - [Build image](#build-image)
  - [Run container](#run-container)
- [RESTAPI endpoint](#restapi-endpoint)
- [RESTAPI docs](#restapi-docs)
- [About the data](#about-the-data)
- [Design docs](#design-docs)
- [Frequently asked questions](docs/faq.md)

## Prerequisites

- **[node](https://nodejs.org/en/download/)** v16.x.x or higher
- **npm** v8.x.x or higher
- [Docker](https://docs.docker.com/get-docker/) 20.x.x or higher
- Optional
  - **[jq](https://stedolan.github.io/jq/)** (for formatting your JSON outputs) 
    - [How to install 'jq' on your machine](https://stedolan.github.io/jq/download/)
    - [Try jq online](https://jqplay.org/)

## Local/native environment

### Install app

At your command prompt, type the below commands:

```bash
$ git clone https://gitlab.com/ANDigital/hamilton/myExpressApp.git
$ cd myExpressApp
$ npm install
```
or 

```bash
$ git clone git@gitlab.com:ANDigital/hamilton/myExpressApp.git
$ cd myExpressApp
$ npm install
```

### Run app

Go into the root folder of the project, and also make sure no instance of the app is running on this machine before running the below:

```bash
$ npm start
```

The above runs the app and listens on port 8080, if this port is not available or if you wish to run it on a different port then do the below:

```bash
$ PORT=<another port number> npm start
```

Once the server is up and running, you can also take a look at the [RESTAPI endpoint](#restapi-endpoint) and [RESTAPI docs](#restapi-docs) resources to see how to call the endpoint in your application or from the command-line, and what results to expect.

[Back to ToC](#a-simple-nodejs-backend-server)

## Docker environment

This section expects familiarity about Docker concepts and using Docker. Use docker image builds as an additional way to check for application development time and runtime reliability, integrity and portability in an isolated environment.

Ensure that Docker is running and you see the blue-whale icon on your taskbar or in your activity/process manager, before going further to the next sub-section.

### Build image

**Note:** if your machine runs a client like `Netskope`, then you may need to _disable_ this client before building the image and _enable_ it again after build is completed. Or else the container fails from being build. See [faq](docs/faq.md) to find out why this could be happening.

```bash
$ ./docker-runner.sh --buildImage
```

*Please build the image the first time and rebuild it each time the project changes in order to be in sync with the changes.*

### Run container

Go into the root folder of the project, and also make sure no instance of the app is running on this machine before running the below:

```bash
$ ./docker-runner.sh --runContainer
```

The above runs the container and listens on port 8080, if this port is not available or if you wish to run it on a different port then do the below:

```bash
$ ./docker-runner.sh --hostport <another port number>   --runContainer
```

Once the server is up and running, you can also take a look at the [RESTAPI endpoint](#restapi-endpoint) and [RESTAPI docs](#restapi-docs) resources to see how to call the endpoint in your application or from the command-line, and what results to expect.

### Other actions

Find out other options available via the below command:

```bash
$ ./docker-runner.sh --help
```

You can see how you can debug a container of detach a container after running it.

[Back to ToC](#a-simple-nodejs-backend-server)

## RESTAPI endpoint

You can access the RESTAPI endpoint via the command-line by doing the below.

Open another terminal window and try this out:

```bash
$ curl localhost:8080/bios

or

$ curl 127.0.0.1:8080/bios
```

and you get an output like this:

```json
[{"urn":"48204d75-38d4-4556-924e-3ea79a088bd8","slug":"zachary-cook","name":"Zachary Cook","andTitle":"World Foodie","gender":"Male","pronoun":"He/Him","profileUrl":"https://luna.and-digital.com/profile/zcook","business_unit":"Club Hamilton","squad":"Nova","bio":{"jobTitle":"Produce Analyst","overview":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","skillCategories":[{"category":"Behaviours","skills":["Communicate with empathy","Delighting the client"]},{"category":"Engineering","skills":["Code quality","Pair programming"]},{"category":"Delivery","skills":["Agile and Lean practices","Budget management"]},{"category":"Product","skills":["Product analysis","Product discovery"]},{"category":"Experience Design","skills":["Prototyping","UI and visual design"]}]}}, 
.
.
.
<-- SNIPPED -->
.
.
. 
{"urn":"46946e87-fa50-4cb8-b879-8e1a117cbf66","slug":"adele-thompson","name":"Adele Thompson","andTitle":"Popcorn Artist","gender":"Female","pronoun":"She/Her","profileUrl":"https://luna.and-digital.com/profile/a_thompson","business_unit":"Cloud S&Y","squad":"Cirrus","bio":{"jobTitle":"Principal Engineer","overview":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.","skillCategories":[{"category":"Behaviours","skills":["Communicate with empathy","Delighting the client"]},{"category":"Engineering","skills":["Code quality","Pair programming"]},{"category":"Delivery","skills":["Agile and Lean practices","Budget management"]},{"category":"Product","skills":["Product analysis","Product discovery"]},{"category":"Experience Design","skills":["Prototyping","UI and visual design"]}]}}]%
```

or using `jq` to get formatted JSON output

```bash
$ curl localhost:8080/bios | jq

or

$ curl 127.0.0.1:8080/bios | jq
```

and you get an output like this:

```json
[
  {
    "urn": "48204d75-38d4-4556-924e-3ea79a088bd8",
    "slug": "zachary-cook",
    "name": "Zachary Cook",
    "andTitle": "World Foodie",
    "gender": "Male",
    "pronoun": "He/Him",
    "profileUrl": "https://luna.and-digital.com/profile/zcook",
    "business_unit": "Club Hamilton",
    "squad": "Nova",
    "bio": {
      "jobTitle": "Produce Analyst",
      "overview": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      "skillCategories": [
        {
          "category": "Behaviours",
          "skills": [
            "Communicate with empathy",
            "Delighting the client"
          ]
        },
        {
          "category": "Engineering",
          "skills": [
            "Code quality",
            "Pair programming"
          ]
        },
        {
          "category": "Delivery",
          "skills": [
            "Agile and Lean practices",
            "Budget management"
          ]
        },
        {
          "category": "Product",
          "skills": [
            "Product analysis",
            "Product discovery"
          ]
        },
        {
          "category": "Experience Design",
          "skills": [
            "Prototyping",
            "UI and visual design"
          ]
        }
      ]
    }
  },
  .
  .
  .
  <-- SNIPPED -->
  .
  .
  .
  {
    "urn": "46946e87-fa50-4cb8-b879-8e1a117cbf66",
    "slug": "adele-thompson",
    "name": "Adele Thompson",
    "andTitle": "Popcorn Artist",
    "gender": "Female",
    "pronoun": "She/Her",
    "profileUrl": "https://luna.and-digital.com/profile/a_thompson",
    "business_unit": "Cloud S&Y",
    "squad": "Cirrus",
    "bio": {
      "jobTitle": "Principal Engineer",
      "overview": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      "skillCategories": [
        {
          "category": "Behaviours",
          "skills": [
            "Communicate with empathy",
            "Delighting the client"
          ]
        },
        {
          "category": "Engineering",
          "skills": [
            "Code quality",
            "Pair programming"
          ]
        },
        {
          "category": "Delivery",
          "skills": [
            "Agile and Lean practices",
            "Budget management"
          ]
        },
        {
          "category": "Product",
          "skills": [
            "Product analysis",
            "Product discovery"
          ]
        },
        {
          "category": "Experience Design",
          "skills": [
            "Prototyping",
            "UI and visual design"
          ]
        }
      ]
    }
  }
]
```
[Back to ToC](#a-simple-nodejs-backend-server)

## RESTAPI docs

We can get access to Swagger generated docs showing how the REST API endpoints are defined and can be accessed, by opening http://localhost:8080/api-docs in the browser.

We can see something like this in the browser:
![Swagger UI in the browswer ](./docs/swagger-ui-in-the-browser.jpg)

## About the data

The data contained in this repo is dummy data generated using this website: [Dummy data dataset generator](https://smalldev.tools/test-data-generator-online).

## Design docs

See [Data Models](https://gitlab.com/ANDigital/hamilton/andi-bio-directory-v2/-/wikis/Data%20Models) on the project wiki.

[Back to ToC](#a-simple-nodejs-backend-server)