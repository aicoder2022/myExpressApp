# Frequently asked questions

- [Security](#security)
  - [Docker + node.js](#docker--nodejs)
  - [RESTAPI: CORS (Cross-Origin Resource Sharing)](#restapi-cors-cross-origin-resource-sharing)
- [Troubleshooting](#troubleshooting)

## Security

### Docker + node.js

- Why does building a docker container for `node.js` fail with the error "request to https://registry.npmjs.org/yargs/-/yargs-3.10.0.tgz failed, reason: self signed certificate in certificate chain"?

When a firewall or a client like `netskope` is in between your docker instance and the `npmjs` registry, it could intercept the SSL/TLS communication and cause the authentication to fail, see [SO discussion](https://stackoverflow.com/questions/58401781/ssl-fails-with-all-hosts-ssl-certificate-problem-self-signed-certificate-in-c):

> ...<snipped>... Netskope client doesn't itself inspect ssl traffic but it breaks the SSL traffic going directly to Destination by acting as a proxy and present its own certificate and sends traffic to Netskope proxy for ssl inspection.

Hence when we run a command-like this inside the container:

```bash
$ npm install
```

After some waiting, we would see an error like this:

```bash
...<snipped>...
#12 72.01 npm ERR! code SELF_SIGNED_CERT_IN_CHAIN
#12 72.01 npm ERR! errno SELF_SIGNED_CERT_IN_CHAIN
#12 72.01 npm ERR! request to https://registry.npmjs.org/yargs/-/yargs-3.10.0.tgz failed, reason: self signed certificate in certificate chain
#12 72.02
#12 72.02 npm ERR! A complete log of this run can be found in:
#12 72.02 npm ERR!     /root/.npm/_logs/[some-date]-debug-0.log
```

A couple of ways to test out if you can make secure `http` connections to the outside world from inside the container are these:

```bash
$ openssl s_client -showcerts -connect registry.npmjs.org:443

OR

$ curl "https://registry.npmjs.org/"
```

Also try this from outside the container, to see the difference.

The solution to the above issue is to temporarily _disable_ the `Netskope` client:

![Netskope client enable/disable menu](docs/netskope-enable-disable-menu.png)

### RESTAPI: CORS (Cross-Origin Resource Sharing)

- How do we know if the backend server (or remote endpoint) is sending us headers with the CORS header(s) in it:_

```bash
$ curl -I -vvv [endpoint]
$ curl -I -vvv google.com
```

and see an output similar to this (for google.com):

```bash
*   Trying 142.250.179.238:80...
* Connected to google.com (142.250.179.238) port 80 (#0)
> HEAD / HTTP/1.1
> Host: google.com
> User-Agent: curl/7.79.1
> Accept: */*
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 301 Moved Permanently
HTTP/1.1 301 Moved Permanently
< Location: http://www.google.com/
Location: http://www.google.com/
< Content-Type: text/html; charset=UTF-8
Content-Type: text/html; charset=UTF-8
< Date: Wed, 27 Apr 2022 13:02:36 GMT
Date: Wed, 27 Apr 2022 13:02:36 GMT
< Expires: Fri, 27 May 2022 13:02:36 GMT
Expires: Fri, 27 May 2022 13:02:36 GMT
< Cache-Control: public, max-age=2592000
Cache-Control: public, max-age=2592000
< Server: gws
Server: gws
< Content-Length: 219
Content-Length: 219
< X-XSS-Protection: 0
X-XSS-Protection: 0
< X-Frame-Options: SAMEORIGIN
X-Frame-Options: SAMEORIGIN

<
* Connection #0 to host google.com left intact
```

OR,

do the below:

```bash
$ curl -I -vvv localhost:8080/bios
```
and see an output similar to this (for localhost:8080/bios):

```bash
*   Trying 127.0.0.1:8080...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> HEAD /bios HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.79.1
> Accept: */*
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
HTTP/1.1 200 OK
< X-Powered-By: Express
X-Powered-By: Express
< Access-Control-Allow-Origin: *
Access-Control-Allow-Origin: *
< Content-Type: application/json; charset=utf-8
Content-Type: application/json; charset=utf-8
< Content-Length: 11711
Content-Length: 11711
< ETag: W/"2dbf-fTvf3HlLy0Jke5C+DsRlG5MhYiA"
ETag: W/"2dbf-fTvf3HlLy0Jke5C+DsRlG5MhYiA"
< Date: Wed, 27 Apr 2022 12:10:14 GMT
Date: Wed, 27 Apr 2022 12:10:14 GMT
< Connection: keep-alive
Connection: keep-alive
< Keep-Alive: timeout=5
Keep-Alive: timeout=5
```

We are looking for a header like `Access-Control-Allow-Origin: *`, the presence of it means it's enabled and also says what filters have been applied to allow/disallow CORS for the caller/end-client.

## Troubleshooting

- How do I know a specific port is available for my application to run? Or it seems port 8080 has been used up by another application how to find out which one?

Try this out to see which application is occupying the port you wish to run your app on:

```bash
$ lsof -i:<port>
```
`<port>` is placejholder for the port.

OR

```bash
$ lsof -i -P | grep LISTEN
```

OR

```bash
$ lsof -Pan -c node -i
```

Also see
https://stackoverflow.com/questions/2179187/application-path-and-listening-port or https://search.brave.com/search?q=netstat+show+app+name+and+port+number to find out more about this topic.

- How do I know a docker container is occupying my port?

Try this to see if there's one or more containers running occupying the port you are looking for:

```bash
$ docker ps
```

You get this output:

```bash
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

OR, something like this, with the port number shown in the output:

```bash
CONTAINER ID   IMAGE                               COMMAND                  CREATED         STATUS         PORTS                    NAMES
bdecf623ac91   aicoder2022/my_express_app:latest   "docker-entrypoint.s…"   2 minutes ago   Up 2 minutes   0.0.0.0:8080->8080/tcp   tempted_swirles
```
OR

```bash
$ lsof -i -P | grep 8080
```

OR

```bash
$ lsof -Pan -c com.docker -i
```

---

[Back to the README](README.md)