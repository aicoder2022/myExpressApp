const request = require('supertest');
const appEndpoint = require('../app');

describe('GET /bios', () => {
    it("should fetch all employees' bios", async () => {
        // given, when: the endpoint /bios is available, 
        //        and it is called
        const response = await request(appEndpoint).get('/bios');

        // then: is successful, and expected result is returned
        expect(response.statusCode).toEqual(200);
        expect(response.body).not.toBeNull();
        expect(response.body.length).toBe(12);
        expect(response.body[0]).toHaveProperty('urn');
    })
});

async function endpointNotFoundExceptionHandling() {
    // given, when: any unavailable/unknown endpoint is called
    const response = await request(appEndpoint).get('/');

    // then: it is not is successful, with an error (404 status code)
    expect(response.statusCode).toEqual(404);

    return;
}

describe('GET / (not defined for this backend)', () => {
    it('should return a 404 error', async () => endpointNotFoundExceptionHandling())
});

describe('GET /unknown-endpoint', () => {
    it('should return a 404 error', async () => endpointNotFoundExceptionHandling())
});