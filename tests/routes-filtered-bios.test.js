const request = require('supertest');
const appEndpoint = require('../app');

describe('GET /bios?filter=Luke', () => {
    it("should fetch all employees' bios containing Luke in the name", async () => {
        // given, when: the endpoint /bios?filter=<string> is available, 
        //        and it is called
        const response = await request(appEndpoint).get('/bios?filter=Luke');

        // then: is successful, and expected result is returned
        expect(response.statusCode).toEqual(200);
        expect(response.body).not.toBeNull();
        expect(response.body.length).toBe(1);
        expect(response.body[0]).toHaveProperty('name', 'Luke Saunders');
    })
});

describe('GET /bios?filter=lUke (case-insensitive)', () => {
    it("should fetch all employees' bios with lUke in the name", async () => {
        // given, when: the endpoint /bios?filter=<string> is available,
        //        and it is called
        const response = await request(appEndpoint).get('/bios?filter=lUke');

        // then: is successful, and expected result is returned
        expect(response.statusCode).toEqual(200);
        expect(response.body).not.toBeNull();
        expect(response.body.length).toBe(1);
        expect(response.body[0]).toHaveProperty('name', 'Luke Saunders');
    })
});

describe('GET /bios?filter=des (case-insensitive and two search hits)', () => {
    it("should fetch all employees' bios with 'des' in the name or 'des' in their job title", async () => {
        // given, when: the endpoint /bios?filter=<string> is available,
        //        and it is called
        const response = await request(appEndpoint).get('/bios?filter=des');

        // then: is successful, and expected result is returned
        expect(response.statusCode).toEqual(200);
        expect(response.body).not.toBeNull();
        expect(response.body.length).toBe(2);
        expect(response.body[0]).toHaveProperty('name', 'Desmond Smith');
        expect(response.body[1]['bio']).toHaveProperty('jobTitle', 'Designer');
    })
});

describe('GET /bios?filter=Unknown', () => {
    it("should NOT fetch any employees' bios for an unknown filter term", async () => {
        // given, when: the endpoint /bios?filter=<string> is NOT available, 
        //        and it is called
        const response = await request(appEndpoint).get('/bios?filter=Unknown');

        // then: is successful, and expected result is returned
        expect(response.statusCode).toEqual(200);
        expect(response.body).toEqual({});
    })
});

describe('GET /bios?filter=developer (two search hits)', () => {
    it("should fetch all employees' bios with jobTitle as developers", async () => {
        // given, when: the endpoint /bios?filter=<string> is available,
        //        and it is called
        const response = await request(appEndpoint).get('/bios?filter=developer');

        // then: is successful, and expected result is returned
        expect(response.statusCode).toEqual(200);
        expect(response.body).not.toBeNull();
        expect(response.body.length).toBe(2);
        expect(response.body[0]['bio']).toHaveProperty('jobTitle', 'Product Developer');
        expect(response.body[1]['bio']).toHaveProperty('jobTitle', 'Product Developer');
    })
});

describe('GET /bios?filter=Kanban (three search hits)', () => {
    it("should fetch all employees' bios where skill starts with Kanban", async () => {
        // given, when: the endpoint /bios?filter=<string> is available,
        //        and it is called
        const expectedResult = [
            {
                "category": "Behaviours",
                "skills": ["Communicate with empathy", "Delighting the client"]
            },
            {
                "category": "Engineering",
                "skills": ["Code quality", "Pair programming"]
            },
            {
                "category": "Delivery",
                "skills": ["Kanban practices", "Budget management"]
            },
            {
                "category": "Product",
                "skills": ["Product analysis", "Product discovery"]
            },
            {
                "category": "Experience Design",
                "skills": ["Prototyping", "UI and Design testing"]
            }
        ];
        const response = await request(appEndpoint).get('/bios?filter=Kanban');

        // then: is successful, and expected result is returned
        expect(response.statusCode).toEqual(200);
        expect(response.body).not.toBeNull();
        expect(response.body.length).toBe(3);
        expect(response.body[0]['bio']['skillCategories']).toEqual(
            expect.objectContaining(expectedResult)
        );
        expect(response.body[1]['bio']['skillCategories']).toEqual(
            expect.objectContaining(expectedResult)
        );
        expect(response.body[2]['bio']['skillCategories']).toEqual(
            expect.objectContaining(expectedResult)
        );
    })
});