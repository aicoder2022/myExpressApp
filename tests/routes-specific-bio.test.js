const request = require('supertest');
const appEndpoint = require('../app');

describe('GET /:slug', () => {
    it("should fetch the specific employee's bio by slug", async () => {
        // given, when: the endpoint /:slug is available, 
        //        and it is called
        slug = "reece-stevens";
        const response = await request(appEndpoint).get(`/${slug}`);

        // then: call is successful, and expected result is returned
        expect(response.statusCode).toEqual(200);
        expect(response.body).not.toBeNull();
        expect(response.body.length).toBe(1);
        expect(response.body[0]['slug']).toBe(slug);
    })
});

describe('GET /:slug (slug in all-caps)', () => {
    it("should fetch the specific employee's bio by slug", async () => {
        // given, when: the endpoint /:slug is available,
        //        and it is called with slug in in all-caps
        slug = "REECE-STEVENS";
        const response = await request(appEndpoint).get(`/${slug}`);

        // then: call is successful, and expected result is returned
        expect(response.statusCode).toEqual(200);
        expect(response.body).not.toBeNull();
        expect(response.body.length).toBe(1);
        expect(response.body[0]['slug']).toBe(slug);
    })
});

describe('GET /:slug (slug in mixed-case: caps and small)', () => {
    it("should fetch the specific employee's bio by slug", async () => {
        // given, when: the endpoint /:slug is available,
        //        and it is called with slug is in mixed-case
        slug = "Reece-Stevens";
        const response = await request(appEndpoint).get(`/${slug}`);

        // then: call is successful, and expected result is returned
        expect(response.statusCode).toEqual(200);
        expect(response.body).not.toBeNull();
        expect(response.body.length).toBe(1);
        expect(response.body[0]['slug']).toBe(slug);
    })
});

describe('GET /:slug - for an unknown or invalid slug', () => {
    it("should return empty result ({}) with a 404 error as response code", async () => {
        // given, when: the endpoint /:slug is available, 
        //        and it is called
        slug = "unknown-slug";
        const response = await request(appEndpoint).get(`/${slug}`);

        // then: call fails successful, and expected result is returned
        expect(response.statusCode).toEqual(404);
        expect(response.body).toEqual({});
    })
});