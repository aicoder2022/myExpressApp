const request = require('supertest');
const appEndpoint = require('../app');

describe("All employees' bios endpoint (GET /bios)", () => {
    it('should return CORS headers', async () => {
        // given, when: endpoint is called
        const response = await request(appEndpoint).get('/bios');
        
        // then: it returns call with the CORS headers
        expect(response.statusCode).toEqual(200);
        expect(response.header).toHaveProperty('access-control-allow-origin', '*');
    })
});

describe("Specific employee's bio by slug endpoint (GET /:slug)", () => {
    it('should return CORS headers', async () => {
        // given, when: endpoint is called
        slug = 'jason-gray';
        const response = await request(appEndpoint).get(`/${slug}`);

        // then: it returns call with the CORS headers
        expect(response.statusCode).toEqual(200);
        expect(response.header).toHaveProperty('access-control-allow-origin', '*');
    })
});

describe("Filter employees bios using the endpoint (GET /bios?filter='<term>')", () => {
    it('should return CORS headers', async () => {
        // given, when: endpoint is called
        filter = 'developer';
        const response = await request(appEndpoint).get(`/bios?filter=${filter}`);

        // then: it returns call with the CORS headers
        expect(response.statusCode).toEqual(200);
        expect(response.header).toHaveProperty('access-control-allow-origin', '*');
    })
});