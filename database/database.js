var fs = require('fs');
var path = require('path');

var fileLocation = path.join(__dirname, 'employees-bios.json')
var employees_bios = JSON.parse(
  fs.readFileSync(fileLocation, 'utf8')
);

function get_all_employees_bios() {
  return employees_bios;
}

function get_employee_bio_by_slug(slug) {
  return employees_bios.filter(
    (item) => item['slug'] === slug
  );
}

function startsWithInSkills(skillCategories, filterText) {
  var foundSkills = false;
  skillCategories.forEach(skillCategory => {
    skillCategory["skills"].forEach(skill => {
      if (skill.toLowerCase().startsWith(filterText.toLowerCase())) {
         foundSkills = true;
      }
    })
  });
  return foundSkills;
}

function containsInName(name, filterText) {
  return name.toLowerCase().includes(filterText.toLowerCase())
}

function containsInJobTitle(jobTitle, filterText) {
  return jobTitle.toLowerCase().includes(filterText.toLowerCase())
}

function get_filtered_employee_bios(filterText) {
  return employees_bios.filter(
    (item) => containsInName(item['name'], filterText) ||
              containsInJobTitle(item['bio']['jobTitle'], filterText) ||
              startsWithInSkills(item['bio']['skillCategories'], filterText)
  );
}

module.exports = { get_all_employees_bios,
                   get_employee_bio_by_slug,
                   get_filtered_employee_bios };