#!/bin/bash

set -eou pipefail

runContainer() {
	setVariables

	## When run in the console mode (command-prompt available)
	TOGGLE_ENTRYPOINT=""

	echo "";
	echo "Running container ${FULL_IMAGE_NAME} on port ${HOST_PORT}"; echo ""

    set -x
    time docker run                                       \
                --rm                                      \
                ${INTERACTIVE_MODE}                       \
                ${TOGGLE_ENTRYPOINT}                      \
                -p ${HOST_PORT}:${CONTAINER_PORT}         \
                --workdir ${WORKDIR}                      \
                "${FULL_IMAGE_NAME}"                      \
                ${DEBUG_CONTAINER}
    set +x
}

copyFilesToTempFolder() {
    mkdir -p ${TMP_DIR}

    for FILE_OR_FOLDER in .vscode bin database routes *.js* tests smoke-tests
    do
        if [[ ! -e "${FILE_OR_FOLDER}" ]]; then
            echo "WARNING: folder ${FILE_OR_FOLDER} not found. Skipping to next steps."
        else
            cp -r ${FILE_OR_FOLDER} ${TMP_DIR}
        fi
    done
}

buildImage() {
	setVariables
	copyFilesToTempFolder

	echo "Building ${APP_NAME} app image ${FULL_IMAGE_NAME}"; echo ""
	time docker build                                        \
                 --build-arg WORKDIR=${WORKDIR}              \
                 --build-arg PORT=${CONTAINER_PORT}          \
	             -t ${FULL_IMAGE_NAME}                       \
	             "${IMAGES_DIR}/."
	echo "Finished building ${APP_NAME} docker image ${FULL_IMAGE_NAME}"

	cleanup
}

cleanup() {
    rm -fr ${TMP_DIR}
	containersToRemove=$(docker ps --quiet --filter "status=exited")
	if [ ! -z "${containersToRemove}" ]; then
	    echo; echo "Remove any stopped container from the local registry"
	    docker rm ${containersToRemove} || true
    fi

	imagesToRemove=$(docker images --quiet --filter "dangling=true")
    if [ ! -z "${imagesToRemove}" ]; then
	    echo; echo "Remove any dangling images from the local registry"
	    docker rmi -f ${imagesToRemove} || true
    fi
}

getContainerID() {
    setVariables
    echo $(docker ps -q --filter="ancestor=${FULL_IMAGE_NAME}" || true)
}

stopContainer() {
    setVariables
    CONTAINER_ID="$(getContainerID)"
    if [[ ! -z "${CONTAINER_ID}" ]]; then
        echo "Shutting down container ${CONTAINER_ID} (on port ${HOST_PORT}) created from ${FULL_IMAGE_NAME}."
        time docker stop "${CONTAINER_ID}"
        echo "Container ${CONTAINER_ID} stopped."
    else
        echo "No container related to ${FULL_IMAGE_NAME} are running."
    fi
}

showLogs() {
    docker logs "$(getContainerID)"
}

runSmokeTests() {
    sh ./smoke-tests/run-api-test.sh
    sh ./smoke-tests/run-swagger-docs-test.sh
}

showUsageText() {
    echo ""
    cat << HEREDOC
       Usage: $0 
                            --detach
                            --hostport [1024-65535]
                            --cleanup
                            --buildImage
                            --debug_mode
                            --runContainer
                            --help
       --detach              run container and detach from it,
                             return control to console
       --hostport            specify an available port between 0 and 65535,
                             handy when running multiple Jupyter sessions.
                             (default: 8080)
       --cleanup             (command action) remove exited containers and
                             dangling images from the local repository
       --buildImage          (command action) build the docker image
       --debug_mode          run container and enter the container,
                             return control to console inside the container
       --runContainer        (command action) run the docker image as a docker container
       --showLogs            (command action) show docker logs of the running container
       --stopContainer       (command action) stop the running docker container
       --runSmokeTests       (command action) runs the smoke tests shell scripts in the smoke-tests folder
       --help                shows the script usage help text

       How to debug a container:
           $0 --debug_mode --runContainer

       How to detach from a container after running it:
           $0 --detach --runContainer

       How to run on a different port:
           $0 --hostport [PORT NO] --runContainer
      e.g. $0 --hostport 1234 --runContainer

HEREDOC

	exit 1
}

setVariables() {
	IMAGE_NAME=${IMAGE_NAME:-my_express_app}
	IMAGE_VERSION=${IMAGE_VERSION:-'latest'}
	FULL_DOCKER_TAG_NAME="${DOCKER_USER_NAME}/${IMAGE_NAME}"
    FULL_IMAGE_NAME=${FULL_DOCKER_TAG_NAME}:${IMAGE_VERSION}
}

#### Start of script
TMP_DIR="tmp"
SCRIPT_CURRENT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
IMAGES_DIR="${SCRIPT_CURRENT_DIR}"

FULL_DOCKER_TAG_NAME=""
DOCKER_USER_NAME="${DOCKER_USER_NAME:-aicoder2022}"

WORKDIR=/usr/src/app

INTERACTIVE_MODE="--interactive --tty"
TIME_IT="time"
DEBUG_CONTAINER=""

HOST_PORT=8080
CONTAINER_PORT=8080

APP_NAME="myExpressApp"

if [[ "$#" -eq 0 ]]; then
	echo "No parameter has been passed. Please see usage below:"
	showUsageText
fi

while [[ "$#" -gt 0 ]]; do case $1 in
  --help)                showUsageText;
                         exit 0;;
  --cleanup)             cleanup;
                         exit 0;;
  --detach)              INTERACTIVE_MODE="--detach";
                         TIME_IT="";;
  --debug_mode)          DEBUG_CONTAINER="/bin/sh";;
  --hostport)            HOST_PORT=${2:-${HOST_PORT}};
                         shift;;
  --buildImage)          buildImage;
                         exit 0;;
  --runContainer)        runContainer;
                         exit 0;;
  --showLogs)            showLogs;
                         exit 0;;
  --stopContainer)       stopContainer;
                         exit 0;;
  --runSmokeTests)       runSmokeTests;
                         exit 0;;
  *) echo "Unknown parameter passed: $1";
     showUsageText;
esac; shift; done

if [[ "$#" -eq 0 ]]; then
	echo "No command action passed in as parameter. Please see usage below:"
	showUsageText
fi